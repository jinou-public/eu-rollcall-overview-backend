//package de.jinou.ero.backend.restapi;
//
//import de.jinou.pub.eurollcalloverview.api.client.api.EpDocumentsApi;
//import de.jinou.pub.eurollcalloverview.api.cmsbackend.api.DocumentsApi;
//import jakarta.validation.Valid;
//import jakarta.validation.constraints.Min;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.util.List;
//
////@Controller
//public class DocumentController implements DocumentsApi {
//
//    EpDocumentsApi epDocumentsApi;
//
//    public DocumentController() {
//        epDocumentsApi = new EpDocumentsApi();
//        epDocumentsApi.getApiClient().setBasePath("https://data.europarl.europa.eu/api/v2");
////        ?work-type=VOTE_ROLLCALL_PLENARY&format=application%2Fld%2Bjson&offset=450&limit=50
//    }
//
//    @Override
//    public Mono<ResponseEntity<Void>> getDocuments(List<String> workType, String format, Integer offset, Integer limit, final ServerWebExchange exchange) {
//
//        String response = getString(filename);
//
////        return webClient.get()
////                .uri(filename)
////                .retrieve()
////                .bodyToMono(String.class)
////                .flatMap(response -> {
////                    // Zwischenspeichern des Inhalts
////                    String documentContent = new String(response);
////                    // Optional: Ausgabe des zwischengespeicherten Inhalts zu Debugging-Zwecken
////                    // Rückgabe des zwischengespeicherten Inhalts
////                    return Mono.just(ResponseEntity.ok().body(documentContent));
////                });
//        return Mono.just(ResponseEntity.ok().body(response));
//    }
//
//    private static String getString(String filename) throws IOException {
////        URL url = new URL("https://www.europarl.europa.eu/doceo/document/"+ filename);
//        URL url = new URL("https://data.europarl.europa.eu/api/v2/documents?work-type=VOTE_ROLLCALL_PLENARY&format=application%2Fld%2Bjson&offset=450&limit=50");
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//
//        // Optional - Set headers or cookies if needed
//        // conn.setRequestProperty("headerName", "headerValue");
//
//        conn.setRequestMethod("GET");
//
//        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//        String xmlResponse = "";
//        String line;
//
//        while ((line = reader.readLine()) != null) {
////            System.out.println(line);
//            xmlResponse+=line;
//        }
//
//        reader.close();
//        conn.disconnect();
//
//        String response = xmlResponse.toString();
//        return response;
//    }
//
//}
