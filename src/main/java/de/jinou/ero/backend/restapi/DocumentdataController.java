package de.jinou.ero.backend.restapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.xml.Jaxb2XmlDecoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Mono;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

@Controller
public class DocumentdataController {

    private final WebClient webClient;

    @Value("${app.maxByteCount}")
    Integer maxByteCount = 3000000;

    @Autowired
    public DocumentdataController(WebClient.Builder webClientBuilder) {
        ExchangeStrategies strategies = ExchangeStrategies.builder()
                .codecs(codecs -> codecs.defaultCodecs().maxInMemorySize(maxByteCount))
                .codecs(configurer ->
                        configurer.defaultCodecs().jaxb2Decoder(new Jaxb2XmlDecoder()))
                .build();
        this.webClient = webClientBuilder.baseUrl("https://www.europarl.europa.eu/doceo/document/")
                .exchangeStrategies(
                strategies
        ).build();
    }

    @GetMapping(value = "/documentdata/{filename}")
    public Mono<ResponseEntity<String>> getDocumentData(@PathVariable String filename) throws IOException {

        String response = getString("https://www.europarl.europa.eu/doceo/document/"+ filename);

//        return webClient.get()
//                .uri(filename)
//                .retrieve()
//                .bodyToMono(String.class)
//                .flatMap(response -> {
//                    // Zwischenspeichern des Inhalts
//                    String documentContent = new String(response);
//                    // Optional: Ausgabe des zwischengespeicherten Inhalts zu Debugging-Zwecken
//                    // Rückgabe des zwischengespeicherten Inhalts
//                    return Mono.just(ResponseEntity.ok().body(documentContent));
//                });
        return Mono.just(ResponseEntity.ok().body(response));
    }


    @GetMapping(value = "/callsdata/{limit}/{offset}")
    public Mono<ResponseEntity<String>> getDocuments(@PathVariable String limit, @PathVariable String offset) throws IOException {

        String response = getString("https://data.europarl.europa.eu/api/v2/documents?work-type=VOTE_ROLLCALL_PLENARY&format=application%2Fld%2Bjson&offset="+offset+"&limit="+limit);

//        return webClient.get()
//                .uri(filename)
//                .retrieve()
//                .bodyToMono(String.class)
//                .flatMap(response -> {
//                    // Zwischenspeichern des Inhalts
//                    String documentContent = new String(response);
//                    // Optional: Ausgabe des zwischengespeicherten Inhalts zu Debugging-Zwecken
//                    // Rückgabe des zwischengespeicherten Inhalts
//                    return Mono.just(ResponseEntity.ok().body(documentContent));
//                });
        return Mono.just(ResponseEntity.ok().body(response));
    }

    private static String getString(String urlstring) throws IOException {
//        URL url = new URL("https://www.europarl.europa.eu/doceo/document/"+ filename);
        URL url = new URL(urlstring);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        // Optional - Set headers or cookies if needed
        // conn.setRequestProperty("headerName", "headerValue");

        conn.setRequestMethod("GET");

        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String xmlResponse = "";
        String line;

        while ((line = reader.readLine()) != null) {
//            System.out.println(line);
            xmlResponse+=line;
        }

        reader.close();
        conn.disconnect();

        String response = xmlResponse.toString();
        return response;
    }

//https://www.europarl.europa.eu/doceo/document/PV-9-2024-04-22-RCV_DE.xml
//https://www.europarl.europa.eu/doceo/document/[]_DE.xml

}
