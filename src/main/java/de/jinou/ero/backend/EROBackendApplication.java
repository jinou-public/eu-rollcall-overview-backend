package de.jinou.ero.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EROBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(EROBackendApplication.class, args);
    }

}
