FROM openjdk:17-jdk
MAINTAINER jinou.de
COPY target/jinou-pub-eurollcalloverview-backend-0.0.1-SNAPSHOT.jar backend.jar
EXPOSE 80
ENTRYPOINT ["java","-jar","/backend.jar"]
